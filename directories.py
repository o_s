import os

genre = ["brief", "gedicht", "verhaal"]
spice = ["character", "dingbat", "image"]
style = {
	"brief" : ["oesters", "nevels", "zon", "dromen", "rivieren", "toekan"],
	"gedicht" : ["vulkanen", "souvenirs", "ster", "golven", "octopus", "rodepepers"],
	"verhaal" : ["valleien", "ijs", "verlangens", "panter", "maan", "stormen"],
}
# genre = style.keys()

for mygenre in genre:
	try:
		os.mkdir(mygenre)
	except:
		pass
	for mystyle in style[mygenre]:
		try:
			os.mkdir(mygenre + "/" + mystyle)
		except:
			pass
# myspace
		for myspice in spice:
			try:
				os.mkdir(mygenre + "/" + mystyle + "/" + myspice)
			except:
				pass
