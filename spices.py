#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A possible use of the odfpy api for the `Openbaar Schrijver' book.
Written by Open Source Publishing.

http://ospublish.constantvzw.org/works/valentine-scripting
http://ospublish.constantvzw.org/works/index.php?/project/publication-templates---openbaar-schrijver/
"""

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

try:
	from odf.opendocument import OpenDocumentText
	from odf.text import P, Span
	from odf.style import Style, TextProperties, ParagraphProperties
	from odf.style import BackgroundImage, GraphicProperties
except ImportError:
	raise

import texts
import stylesheet
from liblove import *

class Spice:
	"""Contains the common methods needed to generate an ODT corresponding to
	a genre/style/spice combination. Its character, dingbat and image
	subclasses add the spice-specific methods.

	This class instantiates the OpenDocumentText class at the end of nested
	loops that look like:

	for genre in ["brief", "verhaal", "gedicht"]:
		for style in genre.styles:
			for spice in spice_list:
				myodt = OpenDocumentText()

	The spice_list is specific to each subclass.

	"""
	def __init__(self, genre, style):
		self.genre = genre
		self.style = style

	def read_stylesheet(self):
		"""Fetches the styles specified by the designer in a CSS-ish
		stylesheet and returns a Style instance for use in an ODT document.

		"""
		stylesheet.text_template[self.genre].update(\
		stylesheet.text_style[self.genre][self.style])
		stylesheet.paragraph_template[self.genre].update(\
		stylesheet.paragraph_style[self.genre][self.style])
		# this paragraph vs text distinction in odf, argh...
		oostyle = Style(name="Body Text", family="paragraph")
		oostyle.addElement(TextProperties(\
		attributes=stylesheet.text_template[self.genre]))
		oostyle.addElement(ParagraphProperties(\
		attributes=stylesheet.paragraph_template[self.genre]))
		return oostyle
	
	def text_frame(self, odt):
		"""Produces the main body of text of the ODT's."""

		### header frame (for letter)
		if self.genre == "brief":
			header(odt)
	
		### textframe
		graphic_style = Style(name="Main Body Frame", family="graphic")
		graphic_properties = GraphicProperties(backgroundcolor="#ffffff", \
		border="10mm double #ffffff")
		graphic_style.addElement(graphic_properties)
		frame = make_frame(odt, graphic_style, "240mm", "170mm", "0mm", "0mm", "1")
		frameframe = frame["frame"]
		textbox = frame["textbox"]
	
		for paragraph in texts.txt[self.genre]:
			kinda_p("Body Text", paragraph, textbox)
	
		odt.text.addElement(frameframe)

	def label(self, odt, spice):
		"""Places a text label in an ODT that identifies it by style and
		spice.

		"""
		## graphic
		graphic_style = Style(name="Label Frame", family="graphic")
		graphic_properties = GraphicProperties()
		graphic_style.addElement(graphic_properties)
		frame = make_frame(odt, graphic_style, "5mm", "50mm", "60mm", "263mm", "1")
		frameframe = frame["frame"]
		textbox = frame["textbox"]
	
		## paragraphic
		paragraphic_style = Style(name="Label Text", family="paragraph")
		text_props = TextProperties(fontsize="11.1pt", \
		fontfamily="Nimbus Sans L", color="#ffffff")
		paragraphic_style.addElement(text_props)
		paragraph_props = ParagraphProperties(backgroundcolor="#000000", \
		textalign="center")
		paragraphic_style.addElement(paragraph_props)
		odt.styles.addElement(paragraphic_style)
		kinda_p(paragraphic_style, spice + " " + self.style, textbox)
	
		odt.text.addElement(frameframe)

class Character(Spice):
	"""These methods are used to generate ODT's where certain words are
	highlighted. The style of highlighting depends on the spice.
	
	"""

	spices = ["sexy", "champagne", "lungo"]
	words = [u"aai", u"aan", u"alleen", u"beloofde", u"bemind", u"benen", u"bewust", u"borst", u"eenzaam", u"elkaar", u"geloofde", u"gesmoord", u"gevoel", u"gezicht", u"giechelend", u"glimlachte", u"haar", u"hand", u"haren", u"hem", u"hen", u"herinner", u"herinneren", u"herinneringen", u"hier", u"hij", u"hoofd", u"hoop", u"huilen", u"ik", u"ja", u"je", u"jij", u"jou", u"jouw", u"kiezen", u"kleurde", u"leven", u"liefde", u"liefdes", u"liefs", u"liefste", u"lieve", u"lippen", u"man", u"me", u"mijn", u"moed", u"moeilijk", u"ogen", u"ons", u"ontmoeting", u"onze", u"opnieuw", u"rituelen", u"roos", u"routine", u"ruggen", u"samen", u"smoorverliefd", u"spijt", u"stotterde", u"streelde", u"tenen", u"troost", u"vergat", u"vergeet", u"vergeten", u"voorzichtig", u"vriend", u"vrienden", u"vrijheid", u"vrouw", u"we", u"wij", u"wil", u"wilde", u"wilden", u"willen", u"ze", u"zelf", u"zij", u"zijn", u"zinnen"]

	# leaving it here because it was built for this spice
	def kinda_p4char_spice(self, paragraphic_style, boldstyle, text, textbox):
		"""Special version of this method from liblove for Character spice class."""
		sectioned = text.split()
		p = P(text=u"", stylename=paragraphic_style)
		try:
			sectioned[-1]
			for i in range(len(sectioned)-1):
				if sectioned[i].lower() in self.words:
					boldpart = Span(stylename=boldstyle, text=sectioned[i] + u" ")
					p.addElement(boldpart)
				else:
					normalpart = Span(text=sectioned[i] + u" ")
					p.addElement(normalpart)
			p.addText(sectioned[-1])
		except IndexError:
			p.addText(u"")
		textbox.addElement(p)

	def character_text_frame(self, odt, spice):
		"""Used instead of the generic `text_frame' method from Spice class."""
		if spice == "sexy":
			boldstyle = Style(name=spice + "Bold", family="text")
			boldprop = TextProperties(fontfamily="diluvienne", fontsize="28pt")
			boldstyle.addElement(boldprop)
			odt.automaticstyles.addElement(boldstyle)
		elif spice == "champagne":
			boldstyle = Style(name=spice + "Bold", family="text")
			boldprop = TextProperties(fontfamily="Cimatics_Trash")
			boldstyle.addElement(boldprop)
			odt.automaticstyles.addElement(boldstyle)
		else:
			boldstyle = Style(name=spice + "Bold", family="text")
			boldprop = TextProperties(fontweight="bold", \
			fontfamily="NotCourierSans", letterspacing="2mm")
			boldstyle.addElement(boldprop)
			odt.automaticstyles.addElement(boldstyle)
	
		### header frame (for letter)
		if self.genre == "brief":
			header(odt)
	
		### textframe
		graphic_style = Style(name="Main Body Frame", family="graphic")
		graphic_properties = GraphicProperties(backgroundcolor="#ffffff", \
		border="10mm double #ffffff")
		graphic_style.addElement(graphic_properties)
		frame = make_frame(odt, graphic_style, "240mm", "170mm", "0mm", "0mm", "1")
		frameframe = frame["frame"]
		textbox = frame["textbox"]
	
		for paragraph in texts.txt[self.genre]:
			self.kinda_p4char_spice("Body Text", boldstyle, paragraph, textbox)
	
		odt.text.addElement(frameframe)

	def loop_over_spices(self):
		"""Iterates over ["sexy", "champagne", "lungo"] and produces 1 ODT
		per cycle.
		
		The names of the ODT's depend on the iteration, as well as the genre
		and style attributes specified in the current instance.

		These are the main loops of the Spice subclasses.

		"""
		for myspice in self.spices:

			myodt = OpenDocumentText()
	
			genredotstyle = self.read_stylesheet()
			myodt.styles.addElement(genredotstyle)
			frame_border(myodt)
	
			self.character_text_frame(myodt, myspice)
	
	
			self.label(myodt, myspice)
	
			myname = self.genre + "/" + self.style + "/character/" +\
			self.genre + "_" + self.style + "_" + myspice
			myodt.save(myname, True)

class Dingbat(Spice):
	"""These methods are used to generate ODT's with a column of dingbats on
	the left. The collection of dingbats that's used depends on the spice.
	
	"""
	dingbats = {"kristallen" : [u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴", u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴", u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴", u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴",u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴", u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴", u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴", u"✭", u"✮", u"✯", u"✰", u"✱", u"✲", u"✳", u"✴"],
		"roze" : [u"✿", u"❀", u"✿", u"❀", u"❁", u"✼", u" ", u"✿", u"❀",u"✿", u"❀", u"❁", u"✼", u" ", u"✿", u"❀", u"✿", u"❀", u"❁", u"✼", u" ", u"✿", u"❀",u"✿", u"❀", u"❁", u"✼", u" ", u"✿", u"❀", u"✿", u"❀", u"❁", u"✼", u" ", u"✿", u"❀",u"✿", u"❀", u"❁", u"✼"],
		"lavendel" : [u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍",u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍",u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍",u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍", u"҈", u"❍"]
	}

	def dingbat_column(self, odt, spice):
		"""Generates the vertical dingbat strip on the left of the ODT's."""

		## graphic
		graphic_style = Style(name="Dingbat Frame", family="graphic")
		graphic_properties = GraphicProperties()
		graphic_style.addElement(graphic_properties)
		frame = make_frame(odt, graphic_style, "240mm", "10mm", "0mm", "0mm", "100")
		frameframe = frame["frame"]
		textbox = frame["textbox"]
	
		## paragraphic
		paragraphic_style = Style(name="Dingbats", family="paragraph")
		text_props = TextProperties(fontsize="14pt", fontfamily="FreeSerif")
		paragraphic_style.addElement(text_props)
		paragraph_props = ParagraphProperties()
		paragraphic_style.addElement(paragraph_props)
		odt.styles.addElement(paragraphic_style)
	
		for i in self.dingbats[spice]:
			kinda_p(paragraphic_style, i, textbox)
	
		odt.text.addElement(frameframe)

	def loop_over_spices(self):
		"""Iterates over ["kristallen", "roze", "lavendel"] and produces 1 ODT
		per cycle.
		
		The names of the ODT's depend on the iteration, as well as the genre
		and style attributes specified in the current instance.

		These are the main loops of the Spice subclasses.

		"""
		spices = self.dingbats.keys()
		for myspice in spices:
			myodt = OpenDocumentText()
	
			genredotstyle = self.read_stylesheet()
			myodt.styles.addElement(genredotstyle)
			frame_border(myodt)
	
			self.text_frame(myodt)
			self.dingbat_column(myodt, myspice)
	
			self.label(myodt, myspice)
	
			myname = self.genre + "/" + self.style + "/dingbat/" +\
			self.genre + "_" + self.style + "_" + myspice
			myodt.save(myname, True)

class ImageSpice(Spice):
	"""Generates ODT's with a background image. The image and its positioning
	depend on the spice.

	"""

	# leaving this method here because it was built for this spice
	def image_spices(self, odt, spice):
		"""Places the background image in the ODT's."""

		## graphic
		graphic_style = Style(name="Border Frame", family="graphic")
		href = odt.addPicture(spice + ".jpg")
		if spice == "chocolade":
			backgroundimage = BackgroundImage(href=href, \
			position="top left", repeat="no")
		elif spice == "koraal":
			backgroundimage = BackgroundImage(href=href, \
			position="bottom right", repeat="no")
		elif spice == "expresso":
			backgroundimage = BackgroundImage(href=href, \
			position="top left", repeat="repeat")
		graphic_properties = GraphicProperties(border="0.5mm double #000000")
		graphic_properties.addElement(backgroundimage)
		graphic_style.addElement(graphic_properties)
		frame = make_frame(odt, graphic_style, "273mm", "194mm", "-12mm", \
		"-12mm", "1")
		frameframe = frame["frame"]
		textbox = frame["textbox"]
	
		## paragraphic
		paragraphic_style = Style(name="Frame Border is Empty", family="paragraph")
		text_props = TextProperties()
		paragraphic_style.addElement(text_props)
		paragraph_props = ParagraphProperties()
		paragraphic_style.addElement(paragraph_props)
		odt.styles.addElement(paragraphic_style)
	
		kinda_p(paragraphic_style, u"", textbox)
	
		odt.text.addElement(frameframe)
	
	def loop_over_spices(self):
		"""Iterates over ["chocolade", "koraal", "expresso"] and produces 1
		ODT per cycle.
		
		The names of the ODT's depend on the iteration, as well as the genre
		and style attributes specified in the current instance.

		These are the main loops of the Spice subclasses.

		"""
		for myspice in ["chocolade", "koraal", "expresso"]:
			myodt = OpenDocumentText()
	
			genredotstyle = self.read_stylesheet()
			myodt.styles.addElement(genredotstyle)
			self.image_spices(myodt, myspice)
	
			self.text_frame(myodt)
	
	
			self.label(myodt, myspice)
	
			myname = self.genre + "/" + self.style + "/image/" +\
			self.genre + "_" + self.style + "_" + myspice
			myodt.save(myname, True)
