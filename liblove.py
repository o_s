#!/usr/bin/env python

"""
A possible use of the odfpy api for the `Openbaar Schrijver' book.
Written by Open Source Publishing.

http://ospublish.constantvzw.org/works/valentine-scripting
http://ospublish.constantvzw.org/works/index.php?/project/publication-templates---openbaar-schrijver/
"""

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import os

from odf.text import P
from odf.style import Style, TextProperties, ParagraphProperties
from odf.style import GraphicProperties
from odf.draw import Frame, TextBox

# general purpose
def directory_structure():
	"""Produces a directory hierarchy for the storage of the ODT's. From the
	top-level directories and then down, the hierarchy looks like:

	genre -- one of brief, gedicht, or verhaal
	style -- relative to genre
	spice -- one of character, dingbat, or image

	Each ODT will live inside of a directory named after its spice. cf the
	`myname' variable of the Spice classes.

	"""
	genre = ["brief", "gedicht", "verhaal"]
	spice = ["character", "dingbat", "image"]
	style = {
		"brief" : ["oesters", "nevels", "zon", "dromen", "rivieren", "toekan"],
		"gedicht" : ["vulkanen", "souvenirs", "ster", "golven", "octopus", "rodepepers"],
		"verhaal" : ["valleien", "ijs", "verlangens", "panter", "maan", "stormen"],
	}
	# genre = style.keys()
	
	for mygenre in genre:
		try:
			os.mkdir(mygenre)
		except:
			pass
		for mystyle in style[mygenre]:
			try:
				os.mkdir(mygenre + "/" + mystyle)
			except:
				pass
	# myspace
			for myspice in spice:
				try:
					os.mkdir(mygenre + "/" + mystyle + "/" + myspice)
				except:
					pass

def make_frame(odt, graphic_style, h, w, ax, ay, z):
	"""Creates an ODF frame, and places an ODF textbox inside of it. Returns
	both the Frame and the TextBox instances. We will often use this function.
	The arguments are:

	odt -- an OpenDocumentText instance
	graphic_style -- a GraphicProperties instance
	h, w, ax, ay, z -- height, width, x-pos, y-pos and z-index, respectively

	From the ODF spec: "within text documents, frames are also used to
	position content outside of the default text flow of a document"

	"""
	odt.styles.addElement(graphic_style)
	myframe = Frame(stylename=graphic_style, height=h, width=w,
					x=ax, y=ay, zindex=z)
	mytextbox = TextBox()
	myframe.addElement(mytextbox)
	return {"frame" : myframe, "textbox" : mytextbox}

def kinda_p(paragraphic_style, text, textbox):
	"""Sets the given `text' string in a paragraph inside of `textbox' styled
	according to `paragraphic_style'.

	"""
	p = P(stylename=paragraphic_style, text=text)
	textbox.addElement(p)

def frame_border(odt):
	"""Produces the frame around the contents of the ODT documents."""
	## graphic
	graphic_style = Style(name="Border Frame", family="graphic")
	graphic_properties = GraphicProperties(border="0.5mm double #000000")
	graphic_style.addElement(graphic_properties)
	frame = make_frame(odt, graphic_style, "273mm", "194mm", "-12mm", "-12mm", "1")
	frameframe = frame["frame"]
	textbox = frame["textbox"]

	## paragraphic
	paragraphic_style = Style(name="Frame Border is Empty", family="paragraph")
	text_props = TextProperties()
	paragraphic_style.addElement(text_props)
	paragraph_props = ParagraphProperties()
	paragraphic_style.addElement(paragraph_props)
	odt.styles.addElement(paragraphic_style)
	kinda_p(paragraphic_style, u"", textbox)

	odt.text.addElement(frameframe)

def header(odt):
	"""Produces the header frame of the `brief' ODT's."""
	## graphic
	#FS ADDED ATLAS STYLE
	header_graphic_style = Style(name="Header Frame", family="graphic")
	header_graphic_properties = GraphicProperties(backgroundcolor="#ffffff", border="10mm double #ffffff")
	header_graphic_style.addElement(header_graphic_properties)
	frame = make_frame(odt, header_graphic_style, "20mm", "170mm", "0mm", "0mm", "2")
	frameframe = frame["frame"]
	textbox = frame["textbox"]
	
	## paragraphic
	header_paragraphic_style = Style(name="Header - Date", family="paragraph")
	atlas_paragraphic_style = Style(name="Header - Atlas", family="paragraph")
	
	header_text_props = TextProperties(fontsize="12pt", fontfamily="DIN_OSP")
	atlas_text_props = TextProperties(fontsize="16pt", fontfamily="OSP-Atlast")
	
	header_paragraphic_style.addElement(header_text_props)
	atlas_paragraphic_style.addElement(atlas_text_props)
	
	header_paragraph_props = ParagraphProperties(textalign="right")
	atlas_paragraph_props = ParagraphProperties(textalign="right")
	header_paragraphic_style.addElement(header_paragraph_props)
	atlas_paragraphic_style.addElement(atlas_paragraph_props)
	odt.styles.addElement(header_paragraphic_style)
	odt.styles.addElement(atlas_paragraphic_style)
	
	kinda_p(header_paragraphic_style, u"Gaasbeek, 13 februari 2050", textbox)
	
	kinda_p(atlas_paragraphic_style, u"Hello World", textbox)
	
	odt.text.addElement(frameframe)
